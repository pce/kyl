kyl
===

          +-------^--+"
         /          / |"
        +---^-.-^--+  |"
        +     °    +  |"
        +          +  |"
        +          + /"
        +---------+"

a tiny ~~icecube~~ library to connect callbacks to URL's.


wishlist
--------

* library example
* support of POST, PUT and DELETE
* CGI


Example
-------

the KylRoute datastructure:

```c
typedef struct {
    uint32_t method;
    const char *pattern;
    pcre *regex;
    pcre_extra *extra;
    func callback;
} KylRoute;
```

connect routes and serve them all:

```c
void foo (int client_fd, char** cgienv)
{
    trace("callback: %s\n", __FUNCTION__);
	char buffer[BUFFER_LEN];
	int length = snprintf(buffer, sizeof(buffer),
		"%s bar buz qux\n",
		__FUNCTION__);
	kyl_print(client_fd, "HTTP/1.0 200 OK\r\n");
	kyl_print(client_fd, "Content-type: text/html\r\n\r\n");
	kyl_print(client_fd, "<html><head><title>foo</title></head>"
			"<body><h1>Foo.</h1>"
			"<pre>");
	if (length) {
		kyl_print(client_fd, buffer);
	}
	kyl_print(client_fd, "</pre></body></html>");
}

void notify(int client_fd, char** cgienv) 
{
	char buffer[BUFFER_LEN];
	kyl_print(client_fd, "HTTP/1.0 200 OK\r\n");
	kyl_print(client_fd, "Content-type: text/html\r\n\r\n");
	kyl_print(client_fd, "<html><head><title>notify</title></head>"
			"<body><h1>when the server runs on the client...</h1>"
			"<pre>");
	int length = snprintf(buffer, sizeof(buffer), 
		"/usr/bin/notify-send '%s' -i notification-message-im -t 10000", "test");
	if (length) {
		system(buffer);
		kyl_print(client_fd, "...fires notify and forget!\n");
	}
	kyl_print(client_fd, "</pre></body></html>");
}

int main(int argc, char **argv)
{

	/*
		http://localhost:6060/notify
		http://localhost:6060/env.sh?x=y&z=9
	*/

	kyl_connect((KylRoute) {
		METHOD_GET, 
		.pattern="\\/notify", 
		NULL,
		NULL,
		.callback=notify
	});
	
	kyl_connect((KylRoute) {
		METHOD_GET, 
		.pattern="\\/foo", 
		NULL,
		NULL,
		.callback=foo
	});
	
	kyl_init();
	kyl_serve(argc, argv);
}
```


