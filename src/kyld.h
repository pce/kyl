#ifndef WITH_CGI
#define WITH_CGI
#endif

#define SERVER_SOFTWARE "kyld"

#ifndef SERVER_VERSION
#define SERVER_VERSION "0.0.1a"
#endif

#define START_MESSAGE SERVER_SOFTWARE" Ver."SERVER_VERSION"\n\n"

#define METHOD_NONE		0x000
#define METHOD_HEAD		0x001
#define METHOD_GET		0x002
#define METHOD_POST		0x004

#define BUFFER_LEN		512
#define MAX_PATH_LEN	4096
#define RCV_TIMEOUT		60

#define OVECCOUNT 30

/* #define KYLD_DAEMON */
#define IPV6_ONLY

/* default Configuration */
#ifndef DOCROOT
#define DOCROOT "./www"
#endif

#ifndef PORT
#define PORT 6060
#endif

#ifndef BACKLOG
#define BACKLOG 128
#endif

#ifndef DEFAULT_INDEX
#define DEFAULT_INDEX "index.html"
#endif

#ifndef ERRORS_DIR
#define ERRORS_DIR "errors"
#endif


/* Debug to stdout */
#ifndef DEBUG
#define DEBUG
#endif

#ifdef DEBUG
#define trace(...) printf(__VA_ARGS__)
#else
#define trace(...)
#endif

#define xsprintf(str, ...) {                    \
    char* tmpstr = (str);                       \
    int size = asprintf(&(str), __VA_ARGS__);   \
    if (size == -1) {                           \
        perror("xsprintf");                     \
        exit(2);                                \
    }                                           \
    free(tmpstr);                               \
}

#define xfree(p) do { free(p); (p) = NULL; } while(0)

typedef struct {
	uint32_t port;
	char *docroot;
	int32_t backlog;
	int32_t verbosity; /* unused */
	char *defaultfile;
	char *errordir;
} KylConfig;

typedef struct {
	int method;
	char *path;
	char *filename;
	char *query;
	char *useragent;
	char *host;
	/* char *cookie; */
	char *content_type;
	int content_length;
} KylRequest;

typedef void (*func) (int client_fd, char** cgienv);

typedef struct {
	uint32_t method;
    const char *pattern;
    pcre *regex;
   	pcre_extra *extra;
    func callback;
} KylRoute;

int  kyl_print(int sockfd, char *buffer);
int  kyl_dispatch(int client_fd, char* url, char** cgienv) ;
void kyl_init();
void kyl_connect(KylRoute route);
