#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <signal.h>
#include <syslog.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <fcntl.h>

#include <pcre.h>

/* openssl or cyassl*/
#ifdef WITH_OSSL
#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"
#include "openssl/rand.h"
#endif

#include "kyld.h"

static KylConfig config;
static KylRequest request;

static KylRoute kylroutes[] = {
    { METHOD_NONE, NULL, NULL, NULL}
};

void init_conf()
{
	config.port = PORT;
	config.docroot = DOCROOT;
	/* queue length of pending connections */
	config.backlog = BACKLOG;
	config.verbosity = 0;
	config.defaultfile = DEFAULT_INDEX;
	config.errordir = ERRORS_DIR;
}

size_t get_file_size(char *filename)
{
	struct stat file_info;
	if (stat(filename, &file_info) == -1)
		return(0);
	return(file_info.st_size);
}

/** tests if file exists and readable */
uint8_t file_exists(char *filename)
{
	FILE *fh;
	if ((fh = fopen(filename, "r")) == NULL) {
		return(0);
	}
	fclose(fh);
	return(1);
}

/** extended malloc, terminates const_cast failure */
void* xmalloc(size_t size)
{
	void* p;
	p = malloc(size);
	if (!p) {
		perror("xmalloc");
		exit(EXIT_FAILURE);
	}
	return p;
}

void error_exit(char *message)
{
	/* trace("%s: %s\n", message, strerror(errno)); */
	perror(message);
	exit(EXIT_FAILURE);
}

void send_error(int client_fd, int http_error)
{
	char buffer[BUFFER_LEN], *filename;
	int length;
	FILE *stream;

	length = snprintf(buffer, sizeof(buffer), "%s/%d.html",
						config.errordir, http_error);
	filename = buffer;
	trace("errorfile:%s\n", filename);
	if ((stream = fopen(filename, "r")) == NULL) {
		/* TODO add: desc[http_error] %m */
		syslog(LOG_NOTICE, "http error: %d.", http_error);
		length = snprintf(buffer, sizeof(buffer),
			"<html><head><title>Error</title></head>"
			"<body><h1>Error (%d).</h1>"
			"</body></html>",
			http_error);
		send(client_fd, "HTTP/1.0 200 OK\r\n", 17, 0);
		send(client_fd, "Content-type: text/html\r\n\r\n", 27, 0);
		send(client_fd, buffer, length, 0);
	} else {
		send(client_fd, "HTTP/1.0 200 OK\r\n", 17, 0);
		const char header_server[] = "Server: "SERVER_SOFTWARE"\r\n";
		send(client_fd, header_server, strlen(header_server), 0);
		send(client_fd, "Content-type: text/html\r\n", 25, 0);
		snprintf(buffer, sizeof(buffer),
			"Content-length: %d\r\n\r\n",
			get_file_size(filename));
		send(client_fd, buffer, strlen(buffer), 0);
		while (!feof(stream)) {
			length = fread(buffer, 1, sizeof(buffer), stream);
			if (length > 0)
				send(client_fd, buffer, length, 0);
		}
		fclose(stream);
	}
}

#ifdef WITH_CGI
char* get_method_str()
{
	if (request.method & METHOD_GET) {
		return("GET");
	} else if (request.method & METHOD_POST) {
		return("POST");
	} else if (request.method & METHOD_HEAD) {
		return("HEAD");
	}
	return("UNKNOWN");
}

char** create_cgienv(struct sockaddr_in *client_addr, char* args)
{
	static char* cgienv[63];
	int index = 0;
	char rp[MAX_PATH_LEN];

	cgienv[index] = NULL;
	xsprintf(cgienv[index], "SERVER_SOFTWARE=%s", SERVER_SOFTWARE);
	index++;
	cgienv[index] = NULL;
	xsprintf(cgienv[index], "SERVER_NAME=%s", SERVER_SOFTWARE);
	index++;
	cgienv[index] = NULL;
	xsprintf(cgienv[index], "HTTP_USER_AGENT=%s", request.useragent);
	index++;
	cgienv[index] = NULL;
	xsprintf(cgienv[index], "REMOTE_ADDR=%s", inet_ntoa(client_addr->sin_addr));
	index++;
	cgienv[index] = NULL;
	xsprintf(cgienv[index], "SERVER_PORT=%d", config.port);
	index++;
	cgienv[index] = NULL;
	xsprintf(cgienv[index], "REQUEST_METHOD=%s", get_method_str());
	index++;
	cgienv[index] = "REDIRECT_STATUS=200";
	index++;
	cgienv[index] = "GATEWAY_INTERFACE=CGI/1.1";
	index++;
	cgienv[index] = "SERVER_PROTOCOL=HTTP/1.0";
	index++;
	cgienv[index] = NULL;
	xsprintf(cgienv[index], "SCRIPT_NAME=%s", request.filename);
	index++;
	cgienv[index] = NULL;
	xsprintf(cgienv[index], "SCRIPT_FILENAME=%s", realpath(request.filename, rp));
	index++;
	if (args && args[0] != '\0') {
		cgienv[index] = NULL;
		xsprintf(cgienv[index], "QUERY_STRING=%s", args);
		index++;
	}
	cgienv[index] = (char*) 0;
	return cgienv;
}
#endif

static char* get_line(int sock_fd)
{
	char* line;
	char buffer[BUFFER_LEN];
	uint i = 0;
	size_t len = sizeof(buffer);
	line = xmalloc(sizeof(buffer));
	while ((i < len-1) &&
		(recv(sock_fd, &(buffer[i]), 1, 0) == 1)) {
		if (buffer[i] == '\n') {
			break;
		} else {
			i++;
		}
	}
	if ((i > 0) && (buffer[i-1] == '\r')) {
		i--;
	}
	buffer[i] = '\0';
	strncpy(line, buffer, sizeof(buffer));
	return line;
}

void handle_http_request(int client_fd, struct sockaddr_in *client_addr)
{
	char buffer[MAX_PATH_LEN];
	/* char mimetype[BUFFER_LEN]; */
	int length;
	FILE *stream;
	char *ext, *qmarkptr, *line, *http_method, *args, *token, *rest;
	char** cgienv;
	struct timeval timeout;
	
	/* initialize vars */
	http_method = (char*) 0;
	line = (char*) 0;
	args = (char*) 0;
	ext = (char*) 0;
	qmarkptr = (char*) 0;

	/* timeout */
	timeout.tv_sec = RCV_TIMEOUT;
	timeout.tv_usec = 0;
	setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));

	line = get_line(client_fd);
	if (line == NULL) {
		send_error(client_fd, 400);
		return;
	}
	
	request.path = (char*) 0;
	request.filename = (char*) 0;
	request.useragent = (char*) 0;
	request.host = (char*) 0;

	// parse_request
	trace("_LINE: %s\n", line);
	token = strtok_r(line, " ", &rest);
	xsprintf(http_method, "%s", token);
	trace("_METHOD: %s\n", http_method);
	token = strtok_r(NULL, " ", &rest);
	xsprintf(request.path, "%s", token);
	trace("_PATH: %s\n", request.path);
	if (strncmp(&request.path[0], "/", 1) == 0) {
		request.filename = strdupa(request.path+1);
	} else {
		request.filename = strdupa(request.path);
		// strncpy(request.filename, request.path, MAX_PATH_LEN);
	}
	trace("_FILENAME: %s\n", request.filename);
	token = strtok_r(NULL, " ", &rest);
	trace("_PROTOCOL: %s\n", token);
	
	if (strcasecmp(http_method, "GET") == 0) {
		request.method |= METHOD_GET;
	} else if (strcasecmp(http_method, "HEAD") == 0) {
		request.method |= METHOD_HEAD;
	} else if (strcasecmp(http_method, "POST") == 0) {
		request.method |= METHOD_POST;
	} else {
		send_error(client_fd, 501);
	}

	if (line) xfree(line);
	trace("get next line\n");

	for (;;) {
		line = get_line(client_fd);
		if (line[0] == '\0') {
			if (line) xfree(line);
			break;
		}
		trace("\t--%s\n", line);
		if (strncasecmp(line, "User-Agent:", 11 ) == 0 ) {
			xsprintf(request.useragent, "%s", line+11);
			trace("_USER_AGENT:%s\n", request.useragent);
		} else if (strncasecmp(line, "Host:", 5 ) == 0 ) {
			// snprintf(request.host, sizeof(buffer), "%s", line+5);
			xsprintf(request.host, "%s", line+5);
			trace("_HOST:%s\n", request.host);
		} else if (strncasecmp(line, "Content-Length:", 15 ) == 0 ) {
			trace("_CONTENT-LENGTH:%s\n", line+15);
			trace("\t-- %s\n", "postdata -[ Content-Type: application/x-www-form-urlencoded");
		}
		if (line) xfree(line);
	}

	qmarkptr = strchr(request.filename, '?');
	if (qmarkptr != NULL) {
		args = qmarkptr+1;
		trace("_ARGS:%s\n", args);
		request.filename[qmarkptr-request.filename] = 0;
		trace("_FILENAME: %s\n", request.filename);
	}

	// TODO	&& !is_dir(request.filename)
	if (strlen(request.filename) == 0 || request.filename[0] == '/') {
		request.filename = config.defaultfile;
	}

	/* instead of access, map binaries to cgi, eg. *|*.cgi */
	if (access(request.filename, X_OK)) {
		trace("%s\n", "filename is not executable");
	} else {
		/* cgi-patch: pipe stdout to socket and execve(filename, cgiargv, cgienv); */
		trace("%s\n", "filename is executable");
#ifdef WITH_CGI
		/* start like minihttpd pipe stdout to socket */
		if (client_fd == STDIN_FILENO || client_fd == STDOUT_FILENO || client_fd == STDERR_FILENO) {
			int newfd = dup2(client_fd, STDERR_FILENO + 1);
			if (newfd >= 0)
				client_fd = newfd;
		}
		// GET
		if (client_fd != STDIN_FILENO) dup2(client_fd, STDIN_FILENO);
		int p[2];
		int r;

		if (pipe(p) < 0)
			send_error(client_fd, 500);
		r = fork();
		if (r < 0)
			send_error(client_fd, 500);
		if (r == 0) {
			trace("%s\n", "Interposer process");
			close(p[1]);
			exit(0);
			close(p[0]);
			if (p[1] != STDOUT_FILENO) {
				(void) dup2(p[1], STDOUT_FILENO);
			}
			if (p[1] != STDERR_FILENO) {
				(void) dup2( p[1], STDERR_FILENO );
			}
			if (p[1] != STDOUT_FILENO && p[1] != STDERR_FILENO) {
				(void) close( p[1] );
			}
		} else {
			trace("%s\n", "request socket is stdout/stderr");
			if (client_fd != STDOUT_FILENO)
				(void) dup2(client_fd, STDOUT_FILENO);
			if (client_fd != STDERR_FILENO)
				(void) dup2(client_fd, STDERR_FILENO);
		}
		/* close syslog. */
		closelog();

		char http_head[] = "HTTP/1.0 200 OK\015\012";
		send(client_fd, http_head, sizeof(http_head), 0);

		/* dircheck? argv Get Params? */
		char* cgiargv[] = { request.filename, NULL };
		cgienv = create_cgienv(client_addr, args);
		(void) execve(request.filename, cgiargv, cgienv);
		/* without env: (void) execv(request.filename, cgiargv); */
		/* execve() only returns on error */
		send_error(client_fd, 500);
		return;
#endif
	}

	if (request.filename[0] == '.' && request.filename[1] == '.'
		&&  request.filename[2] == '/') {
		send_error(client_fd, 400);
		return;
	}

	if ((stream = fopen(request.filename, "r")) == NULL) {
		trace("%s\n", "call create_cgienv");
		// cgienv = create_cgienv(client_addr, args);
		char* _cgienv[4];
		_cgienv[0] = "dummy";
		_cgienv[1] = (char*) 0;
		trace("%s\n", "call kyl_dispatch");
		// char* url = "/foo";
		trace("request.path: %s\n", request.path);
		if (kyl_dispatch(client_fd, request.path, _cgienv)) {
			return;
		}
		/* else 404 */
		trace("error 404: %s\n", request.filename);
		syslog(LOG_WARNING, "error: file not found (404)");
		send_error(client_fd, 404);
		return;
	}


	/*	setsockopt (client_fd, SOL_TCP, TCP_NODELAY, &on, sizeof (on)); */
	/* TCP_CORK option to disable Nagling (for sendfile, write) :
	int on = 1;
	setsockopt(client_fd, SOL_TCP, TCP_CORK, &on, sizeof (on));
	*/

	send(client_fd, "HTTP/1.0 200 OK\r\n", 17, 0);
	send(client_fd, "Server: "SERVER_SOFTWARE"\r\n",
			strlen("Server: "SERVER_SOFTWARE"\r\n"), 0);

	/* TODO return mimetype by function which iterates a map */
	if (
		(strcmp(&(request.filename[strlen(request.filename)-5]), ".html") == 0)
		||
		(strcmp(&(request.filename[strlen(request.filename)-4]), ".htm") == 0)
	) {
		send(client_fd, "Content-type: text/html\r\n", 25, 0);
	} else if (request.path != NULL) {
		ext = strtok(request.path, ".");
		while (ext != NULL) {
			ext = strtok(NULL, ".");
			if (ext != NULL) {
				/* DRY, use struct or apachestyle (sniff filetype) ? */
				trace("%s\n", ext);
				if (strncmp(ext, "jpg", 4) == 0) {
					trace("%s\n", "image/jpeg");
					send(client_fd, "Content-type: image/jpeg\r\n", 26, 0);
					break;
				} else if (strncmp(ext, "gif", 4) == 0) {
					send(client_fd, "Content-type: image/gif\r\n", 25, 0);
					break;
				} else if (strncmp(ext, "png", 4) == 0) {
					trace("%s\n", "image/png");
					send(client_fd, "Content-type: image/png\r\n", 25, 0);
					break;
				} else if (strncmp(ext, "ico", 4) == 0) {
					trace("%s\n", "image/x-icon");
					send(client_fd, "Content-type: image/x-icon\r\n", 28, 0);
					break;
				} else if (strncmp(ext, "css", 4) == 0) {
					send(client_fd, "Content-type: text/css\r\n", 24, 0);
					break;
				} else {
					send(client_fd, "Content-type: application/octet-stream\r\n", 40, 0);
					break;
				}
			}
		}
		// trace("request.path=%s\n", "NULL");
		// request.path = NULL;
		/*
		snprintf(line, sizeof(buffer),
			"Content-type: %s\r\n\r\n",
			mimetype);
		send(client_fd, line, strlen(line), 0);
		*/
	}

	snprintf(line, sizeof(buffer),
		"Content-length: %d\r\n\r\n",
		get_file_size(request.filename));

	send(client_fd, line, strlen(line), 0);
	if (request.method & METHOD_GET || request.method & METHOD_POST) {
		trace("open stream and send: %s\n", request.filename);
		while (!feof(stream)) {
			length = fread(line, 1, sizeof(buffer), stream);
			if (length > 0)
				send(client_fd, line, length, 0);
		}
	}
	fclose(stream);
	/* uncork */
	/*
	on = 0;
	setsockopt(client_fd, SOL_TCP, TCP_CORK, &on, sizeof (on));
	*/
	return;
}



void free_on_exit(void)
{
	// TODO: free_on_exit
	trace("%s\n", "free_on_exit()");
}


int kyl_listen(uint port)
{
	int sock_fd, err;
	struct sockaddr_in6 srv_addr6;
	struct sockaddr_in srv_addr;

#ifdef IPV6_ONLY
	sock_fd = socket(PF_INET6, SOCK_STREAM, 0);
#else
	sock_fd = socket(PF_INET, SOCK_STREAM, 0);
#endif
	if (sock_fd == -1)
		error_exit("["SERVER_SOFTWARE"] socket");

	/* IPv6 */
	srv_addr6.sin6_family = AF_INET6;
	srv_addr6.sin6_port = htons(config.port);
	srv_addr6.sin6_addr = in6addr_any; /* | in6addr_loopback */
	srv_addr6.sin6_flowinfo = 0; // IPv6 flow information
	srv_addr6.sin6_scope_id = 0; // Scope ID

	/* IPv4 */
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(config.port);
	srv_addr.sin_addr.s_addr = INADDR_ANY;

#ifdef IPV6_ONLY
	/* socketaddress in kernel routingtable */
	/*
	getsockname(sock_fd, (struct sockaddr *)&srv_addr6, sizeof(srv_addr6));
	trace("client ip:%s\n", inet6_ntoa(srv_addr6.sin6_addr));
	*/
	err = bind(sock_fd, (struct sockaddr *)&srv_addr6,
			 sizeof(struct sockaddr_in6));
#else
	/*
	getsockname(sock_fd, (struct sockaddr *)&srv_addr, sizeof(srv_addr6));
	trace("client ip:%s\n", inet_ntoa(srv_addr.sin_addr));
	*/
	err = bind(sock_fd, (struct sockaddr *)&srv_addr,
			 sizeof(struct sockaddr_in));
#endif
	if (err == -1)
		error_exit("["SERVER_SOFTWARE"] bind sock");
	setuid(getuid());

	err = listen(sock_fd, config.backlog);
	if (err == -1)
		error_exit("["SERVER_SOFTWARE"] listen");

	return sock_fd;
}


int kyl_serve(int argc, char **argv)
{
    int sock_fd, client_fd;
    /* struct sockaddr_in6 client_addr6; */
    struct sockaddr_in client_addr;
    pid_t pid;
    socklen_t addr_size;

    // atexit(free_on_exit);
    init_conf();

	/* process into daemon: daemon_init(argv[0], PIDFILE, LOG_DAEMON); */
#ifdef KYLD_DAEMON
	if (daemon(1, 1) < 0) {
		syslog(LOG_CRIT, "daemon");
		perror("daemon");
		exit(EXIT_FAILURE);
	}
#endif

	sock_fd = kyl_listen(config.port);

	if (chdir(config.docroot) != 0) {
		printf("%s", config.docroot);
        error_exit("["SERVER_SOFTWARE"] chdir to Document Root");
    }
	/* prevents zombies in the process table */
	signal(SIGCHLD, SIG_IGN);

	trace("%s\n", START_MESSAGE);

	while (1) {
		addr_size = sizeof(struct sockaddr_in);
		client_fd = accept(sock_fd,
				(struct sockaddr *)&client_addr, &addr_size);
		if (client_fd == -1)
			error_exit("["SERVER_SOFTWARE"] accept");

		pid = fork();
		if (pid == -1) {
			error_exit("["SERVER_SOFTWARE"] fork");
			return(EXIT_FAILURE);
		} else if (pid == 0) {
			close(sock_fd);
			handle_http_request(client_fd, &client_addr);
			shutdown(client_fd, SHUT_RDWR);
			close(client_fd);
			return(EXIT_SUCCESS);
		}
		close(client_fd);
	}
	return(EXIT_SUCCESS);
}


int kyl_dispatch(int client_fd, char* url, char** cgienv) 
{
	int rc;
	int ovector[OVECCOUNT];
	int subjectlen = (int)strlen(url);

	trace("url: %s\n", url);

	for (int i = 0; ; i++) {
		if (!kylroutes[i].callback) {
			trace("error: no such route\n");
			break;
		}

		rc = pcre_exec(
		  kylroutes[i].regex, 	/* the compiled pattern */
		  kylroutes[i].extra,   /* extra studied data */
		  url,              	/* the subject string */
		  subjectlen,       	/* the length of the subject */
		  0,                    /* start at offset 0 in the subject */
		  0,                    /* default options */
		  ovector,              /* output vector for substring information */
		  OVECCOUNT);           /* number of elements in the output vector */

		/* Matching failed: handle error cases */
		if (rc < 0) {
			switch (rc) {
				case PCRE_ERROR_NOMATCH: trace("no match\n"); break; 
				default: trace("matching error %d\n", rc); break;
			}
			/* TODO at_exit: Release memory used for the compiled pattern */
			// pcre_free(kylroutes[i].regex);
			continue;
		} else {
			/* Match succeded */
			trace("\nmatch succeeded at offset %d\n", ovector[0]);
			kylroutes[i].callback(client_fd, cgienv);
			return 1;
		}
	}
	return 0;
}


void kyl_init() 
{
	const char *error;
	int erroffset;	

	for (int i = 0; ; i++) {
		if (!kylroutes[i].callback) {
			trace("[kyl_init] registered %d callbacks\n", i);
			break;
		}
		
		if (!kylroutes[i].pattern) continue;
		
		kylroutes[i].regex = pcre_compile(
		  kylroutes[i].pattern,  /* the pattern, TODO append delims here */
		  0,                     /* default options */
		  &error,                /* for error message */
		  &erroffset,            /* for error offset */
		  NULL);                 /* use default character tables */
		  
		// Optimize the regex and TODO free
		// kylroutes[i].extra = pcre_study(kylroutes[i].regex, 0, &error);
		kylroutes[i].extra = NULL;
	}
}


void kyl_connect(KylRoute route)
{
    KylRoute *cur;
    int i = 0;
    for(cur = kylroutes; cur->callback; cur++) {
        i++;
    }
    trace("add route to: %d\n", i);
    // kyl_to_namedparams(route);
    // compile regex here or in init?
    kylroutes[i] = route;
    kylroutes[i + 1] = (KylRoute) {METHOD_NONE, NULL, NULL, NULL};
}


int kyl_print(int sockfd, char *buffer)
{
	int sendbytes = strlen(buffer);
	int	bytes_sent = send(sockfd, buffer, sendbytes, 0);
	if (bytes_sent != -1) {
		return 1;
	}
	return 0;
}

int kyl_printb(int sockfd, char *buffer)
{
	int sendbytes = strlen(buffer);
	int bytes_sent = 0;
	while(sendbytes > 0) {
		bytes_sent = send(sockfd, buffer, sendbytes, 0);
		if (bytes_sent == -1) {
			return 0;
		}
		buffer += bytes_sent;
	}
	return 1;
}

#ifdef WITH_MAIN
void foo (int client_fd, char** cgienv)
{
    trace("callback: %s\n", __FUNCTION__);
	char buffer[BUFFER_LEN];
	int length = snprintf(buffer, sizeof(buffer),
		"%s bar buz qux\n",
		__FUNCTION__);
	kyl_print(client_fd, "HTTP/1.0 200 OK\r\n");
	kyl_print(client_fd, "Content-type: text/html\r\n\r\n");
	kyl_print(client_fd, "<html><head><title>foo</title></head>"
			"<body><h1>Foo.</h1>"
			"<pre>");
	if (length) {
		kyl_print(client_fd, buffer);
	}
	kyl_print(client_fd, "</pre></body></html>");
}

void notify(int client_fd, char** cgienv) 
{
	char buffer[BUFFER_LEN];
	kyl_print(client_fd, "HTTP/1.0 200 OK\r\n");
	kyl_print(client_fd, "Content-type: text/html\r\n\r\n");
	kyl_print(client_fd, "<html><head><title>notify</title></head>"
			"<body><h1>when the server runs on the client...</h1>"
			"<pre>");
	int length = snprintf(buffer, sizeof(buffer), 
		"/usr/bin/notify-send '%s' -i notification-message-im -t 10000", "test");
	if (length) {
		system(buffer);
		kyl_print(client_fd, "...fires notify and forget!\n");
	}
	kyl_print(client_fd, "</pre></body></html>");
}

int main(int argc, char **argv)
{

	/*
		http://localhost:6060/notify
		http://localhost:6060/env.sh?x=y&z=9
	*/

	kyl_connect((KylRoute) {
		METHOD_GET, 
		.pattern="\\/notify", 
		NULL,
		NULL,
		.callback=notify
	});
	
	kyl_connect((KylRoute) {
		METHOD_GET, 
		.pattern="\\/foo", 
		NULL,
		NULL,
		.callback=foo
	});
	
	kyl_init();
	kyl_serve(argc, argv);
}
#endif
